EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:soic multi adapter-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x22_Male J1
U 1 1 5C806E71
P 750 1700
F 0 "J1" H 750 2800 50  0000 C CNN
F 1 "Conn_01x22_Male" H 750 500 50  0000 C CNN
F 2 "Connectors_Samtec:SL-122-X-XX_1x22" H 750 1700 50  0001 C CNN
F 3 "" H 750 1700 50  0001 C CNN
	1    750  1700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x22_Male J3
U 1 1 5C806ED6
P 2750 1700
F 0 "J3" H 2750 2800 50  0000 C CNN
F 1 "Conn_01x22_Male" H 2750 500 50  0000 C CNN
F 2 "Connectors_Samtec:SL-122-X-XX_1x22" H 2750 1700 50  0001 C CNN
F 3 "" H 2750 1700 50  0001 C CNN
	1    2750 1700
	-1   0    0    -1  
$EndComp
$Comp
L Conn_02x22_Top_Bottom J2
U 1 1 5C806F27
P 1700 1700
F 0 "J2" H 1750 2800 50  0000 C CNN
F 1 "Conn_02x22_Top_Bottom" H 1750 500 50  0000 C CNN
F 2 "IPC_SOIC127P:SOIC44P127_2850X1603X300L76X43N_Special" H 1700 1700 50  0001 C CNN
F 3 "" H 1700 1700 50  0001 C CNN
	1    1700 1700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x04_Male J4
U 1 1 5C81BD9D
P 3550 800
F 0 "J4" H 3550 1000 50  0000 C CNN
F 1 "Conn_01x04_Male" H 3550 500 50  0000 C CNN
F 2 "Connectors_Samtec:SL-104-X-XX_1x04" H 3550 800 50  0001 C CNN
F 3 "" H 3550 800 50  0001 C CNN
	1    3550 800 
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03_Male J7
U 1 1 5C81BE62
P 3550 1500
F 0 "J7" H 3550 1700 50  0000 C CNN
F 1 "Conn_01x03_Male" H 3550 1300 50  0000 C CNN
F 2 "Connectors_Samtec:SL-103-X-XX_1x03" H 3550 1500 50  0001 C CNN
F 3 "" H 3550 1500 50  0001 C CNN
	1    3550 1500
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x04_Male J6
U 1 1 5C81BEC5
P 5250 800
F 0 "J6" H 5250 1000 50  0000 C CNN
F 1 "Conn_01x04_Male" H 5250 500 50  0000 C CNN
F 2 "Connectors_Samtec:SL-104-X-XX_1x04" H 5250 800 50  0001 C CNN
F 3 "" H 5250 800 50  0001 C CNN
	1    5250 800 
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x03_Male J9
U 1 1 5C81BF48
P 5250 1500
F 0 "J9" H 5250 1700 50  0000 C CNN
F 1 "Conn_01x03_Male" H 5250 1300 50  0000 C CNN
F 2 "Connectors_Samtec:SL-103-X-XX_1x03" H 5250 1500 50  0001 C CNN
F 3 "" H 5250 1500 50  0001 C CNN
	1    5250 1500
	-1   0    0    -1  
$EndComp
$Comp
L Conn_02x04_Top_Bottom J5
U 1 1 5C81BF9E
P 4350 800
F 0 "J5" H 4400 1000 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 4400 500 50  0000 C CNN
F 2 "IPC_SOT:SOT23-8P65_280X145L45X30N" H 4350 800 50  0001 C CNN
F 3 "" H 4350 800 50  0001 C CNN
	1    4350 800 
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x03_Top_Bottom J8
U 1 1 5C81BFFF
P 4350 1500
F 0 "J8" H 4400 1700 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 4400 1300 50  0000 C CNN
F 2 "IPC_SOT:SOT23-6P95_280X145L45X37N" H 4350 1500 50  0001 C CNN
F 3 "" H 4350 1500 50  0001 C CNN
	1    4350 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 700  950  700 
Wire Wire Line
	950  800  1500 800 
Wire Wire Line
	1500 900  950  900 
Wire Wire Line
	950  1000 1500 1000
Wire Wire Line
	1500 1100 950  1100
Wire Wire Line
	950  1200 1500 1200
Wire Wire Line
	1500 1300 950  1300
Wire Wire Line
	950  1400 1500 1400
Wire Wire Line
	1500 1500 950  1500
Wire Wire Line
	950  1600 1500 1600
Wire Wire Line
	1500 1700 950  1700
Wire Wire Line
	950  1800 1500 1800
Wire Wire Line
	1500 1900 950  1900
Wire Wire Line
	950  2000 1500 2000
Wire Wire Line
	1500 2100 950  2100
Wire Wire Line
	950  2200 1500 2200
Wire Wire Line
	1500 2300 950  2300
Wire Wire Line
	950  2400 1500 2400
Wire Wire Line
	1500 2500 950  2500
Wire Wire Line
	950  2600 1500 2600
Wire Wire Line
	1500 2700 950  2700
Wire Wire Line
	950  2800 1500 2800
Wire Wire Line
	2000 2800 2550 2800
Wire Wire Line
	2550 2700 2000 2700
Wire Wire Line
	2000 2600 2550 2600
Wire Wire Line
	2550 2500 2000 2500
Wire Wire Line
	2000 2400 2550 2400
Wire Wire Line
	2550 2300 2000 2300
Wire Wire Line
	2000 2200 2550 2200
Wire Wire Line
	2550 2100 2000 2100
Wire Wire Line
	2000 2000 2550 2000
Wire Wire Line
	2550 1900 2000 1900
Wire Wire Line
	2000 1800 2550 1800
Wire Wire Line
	2550 1700 2000 1700
Wire Wire Line
	2000 1600 2550 1600
Wire Wire Line
	2550 1500 2000 1500
Wire Wire Line
	2000 1400 2550 1400
Wire Wire Line
	2550 1300 2000 1300
Wire Wire Line
	2000 1200 2550 1200
Wire Wire Line
	2550 1100 2000 1100
Wire Wire Line
	2000 1000 2550 1000
Wire Wire Line
	2550 900  2000 900 
Wire Wire Line
	2000 800  2550 800 
Wire Wire Line
	2550 700  2000 700 
Wire Wire Line
	4150 700  3750 700 
Wire Wire Line
	3750 800  4150 800 
Wire Wire Line
	4150 900  3750 900 
Wire Wire Line
	3750 1000 4150 1000
Wire Wire Line
	4650 1000 5050 1000
Wire Wire Line
	5050 900  4650 900 
Wire Wire Line
	4650 800  5050 800 
Wire Wire Line
	5050 700  4650 700 
Wire Wire Line
	5050 1600 4650 1600
Wire Wire Line
	4650 1500 5050 1500
Wire Wire Line
	5050 1400 4650 1400
Wire Wire Line
	4150 1400 3750 1400
Wire Wire Line
	3750 1500 4150 1500
Wire Wire Line
	4150 1600 3750 1600
$EndSCHEMATC
